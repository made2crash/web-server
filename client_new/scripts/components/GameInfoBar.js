define([
    'react',
    'game-logic/clib',
    'game-logic/GameEngineStore'
], function (
    React,
    Clib,
    Engine
) {
    var D = React.DOM;
    function getState() {
        return {
            totalPlayers: 0,
            bettingPlayers: 0,
            totalBets: 0
        }
    }
    if (!Object.keys) {
        Object.keys = (function () {
            'use strict';
            var hasOwnProperty = Object.prototype.hasOwnProperty,
                hasDontEnumBug = !({ toString: null }).propertyIsEnumerable('toString'),
                dontEnums = [
                    'toString',
                    'toLocaleString',
                    'valueOf',
                    'hasOwnProperty',
                    'isPrototypeOf',
                    'propertyIsEnumerable',
                    'constructor'
                ],
                dontEnumsLength = dontEnums.length;

            return function (obj) {
                if (typeof obj !== 'function' && (typeof obj !== 'object' || obj === null)) {
                    throw new TypeError('Object.keys called on non-object');
                }

                var result = [], prop, i;

                for (prop in obj) {
                    if (hasOwnProperty.call(obj, prop)) {
                        result.push(prop);
                    }
                }

                if (hasDontEnumBug) {
                    for (i = 0; i < dontEnumsLength; i++) {
                        if (hasOwnProperty.call(obj, dontEnums[i])) {
                            result.push(dontEnums[i]);
                        }
                    }
                }
                return result;
            };
        }());
    }
    if (!Object.values) {
        Object.values = (function () {
            return function (obj) {
                if (typeof obj !== 'function' && (typeof obj !== 'object' || obj === null)) {
                    throw new TypeError('Object.values called on non-object');
                }
                var result = [], keys;
                keys = Object.keys(obj);
                keys.forEach(function (key) {
                    result.push(obj[key]);
                });
                return result;
            }
        })();
    }
    return React.createClass({
        displayName: 'GameInfoBar',
        getInitialState: function () {
            return getState()
        },
        componentDidMount: function () {
            Engine.on({
                joined: this._updateData,
                disconnected: this._updateData,
                game_started: this._updateData,
                game_crash: this._updateData,
                game_starting: this._updateData,
                player_bet: this._updateData,
                cashed_out: this._updateData
            });
        },
        componentWillUnmount: function () {
            Engine.off({
                joined: this._updateData,
                disconnected: this._updateData,
                game_started: this._updateData,
                game_crash: this._updateData,
                game_starting: this._updateData,
                player_bet: this._updateData,
                cashed_out: this._updateData
            });
        },

        
        _updateData: function () {
            var players = Engine.playerInfo
            var onlinePlayers = Engine.onlineCount
            var numPlayers = Object.keys(players).length
            var values = Object.values(players)
            var totalBets = 0
            var bettingPlayers = 0
            values.forEach(function (player) {
                if (player.bet) {
                    totalBets += parseFloat(Clib.formatSatoshis(player.bet, 8))
                    bettingPlayers++
                }
            })
            this.setState({
                totalPlayers: onlinePlayers,
                bettingPlayers: bettingPlayers,
                totalBets: totalBets
            })
        },
        render() {
            return D.div({ className: 'game-info-bar'}, [
                D.p({ className: 'game-info', key: 'total-players' }, [
                    'Online: ' + this.state.totalPlayers
                ]),
                D.p({ className: 'game-info', key: 'betting-players' }, [
                    'Playing: ' + this.state.bettingPlayers
                ]),
                D.p({ className: 'game-info', key: 'total-bets' }, [
                    'Betting: ' + this.state.totalBets + ' RVN'
                ])
            ])
        }
    })
});