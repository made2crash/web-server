define([
  'react',
  'stores/ControlsStore',
  'dispatcher/AppDispatcher',
  'constants/AppConstants',
  'actions/ControlsActions'
], function (
  React,
  ControlsStore,
  AppDispatcher,
  AppConstants,
  ControlsActions
) {
  var D = React.DOM;
  return React.createClass({
    displayName: 'ScaleBetButton',
    propTypes: {
      scaleSize: React.PropTypes.string.isRequired,
      engine: React.PropTypes.object
    },
    _scaleBet: function () {
      switch (this.props.scaleSize) {
        case 'half':
          ControlsActions.halfBet();
          break;
        case 'double':
          ControlsActions.doubleBet();
          break;
        case 'min':
          ControlsActions.minBet();
          break;
        case 'max':
          betBal = this.props.engine.balanceSatoshis/1e8;
          gameMax = this.props.engine.maxBet/1e8;
          strBal = String(betBal);
          strMax = String(gameMax);

          if (betBal > gameMax) {
              if (strMax.length > 7) {
                maxBal = strMax.split('.');
                maxNum = maxBal[1].substring(0,5);
                betMax = maxBal[0] + '.' + maxNum;
              } else {
                betMax = gameMax
              }
          } else {
              if (strBal.length > 7) {
                Bal = strBal.split('.');
                decNum = Bal[1].substring(0,5);
                betMax = Bal[0] + '.' + decNum;
              } else {
                betMax = betBal
              }
          }
          ControlsActions.maxBet(betMax);
          break;
      }
    },
    render: function () {
      var self = this;
      let scaleText;
      switch (this.props.scaleSize) {
        case 'half':
          scaleText = '1/2';
          break;
        case 'double':
          scaleText = 'x2'
          break;
        case 'min':
          scaleText = 'Min'
          break;
        case 'max':
          scaleText = 'Max'
          break;
        default:
          throw new Error('Unkown bet scaling size: ' + this.props.scaleSize)
          break;
      }
      return D.button({
          className: 'scale-bet-button',
          onClick: self._scaleBet
        },
        D.span(null, scaleText)
      );
    }
  })
})