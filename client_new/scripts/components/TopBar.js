define([
    'react',
    'game-logic/GameEngineStore',
    'stores/GameSettingsStore',
    'actions/GameSettingsActions',
    'game-logic/clib',
    'screenfull'
], function(
    React,
    Engine,
    GameSettingsStore,
    GameSettingsActions,
    Clib,
    Screenfull //Attached to window.screenfull
) {
    var D = React.DOM;

    function getState() {
        return {
            balanceBitsFormatted: Clib.formatSatoshis(Engine.balanceSatoshis),
            // balanceRVNFormatted: Clib.formatRVN(Engine.balanceSatoshis),
            theme: GameSettingsStore.getCurrentTheme()
        }
    }

    return React.createClass({
        displayName: 'TopBar',

        propTypes: {
            isMobileOrSmall: React.PropTypes.bool.isRequired
        },

        getInitialState: function() {
            var state = getState();
            state.fullScreen = false;
            return state;
        },

        componentDidMount: function() {
            Engine.on({
                joined: this._onChange,
                game_started: this._onChange,
                game_crash: this._onChange,
                cashed_out: this._onChange
            });
            GameSettingsStore.on('all', this._onChange);
        },

        componentWillUnmount: function() {
            Engine.off({
                joined: this._onChange,
                game_started: this._onChange,
                game_crash: this._onChange,
                cashed_out: this._onChange
            });
            GameSettingsStore.off('all', this._onChange);
        },

        _onChange: function() {
            if(this.isMounted())
                this.setState(getState());
        },

        _toggleTheme: function() {
            GameSettingsActions.toggleTheme();
        },

        _toggleFullScreen: function() {
        	window.screenfull.toggle();
            this.setState({ fullScreen: !this.state.fullScreen });
        },

        render: function() {

            let userLogin;
            let mobileBar;
            if(Engine.username) {
                userLogin = D.div({ className: 'user-login' },
                    D.div({ className: 'balance-bits' },
                        D.span(null, 'RVN: '),
                        D.span({ className: 'balance' }, this.state.balanceBitsFormatted )
                    ),
                    D.div({ className: 'username' },
                        D.a({ href: '/account'}, Engine.username
                    ))
                );

                mobileBar = D.div({ className: 'navMobile' },
                D.div({ id: 'navLinks' },
                    D.div({ className: 'balanceBits' },
                        D.span(null, 'RVN: '),
                        D.span({ className: 'balance' }, this.state.balanceBitsFormatted )
                    ),
                    D.a({ href: '/account'}, Engine.username)
                )
            );
            } else {
                userLogin = D.div({ className: 'user-login' },
                    D.div({ className: 'register' },
                        D.a({ href: '/register' }, 'Register' )
                    ),
                    D.div({ className: 'login' },
                        D.a({ href: '/login'}, 'Log in' )
                    )
                );

                mobileBar = D.div({ className: 'navMobile' },
                D.div({ id: 'navLinks' },
                    D.a({ href: '/register' }, 'Register' ),
                    D.a({ href: '/login'}, 'Log in' )
                )
            );
            }

            return D.div({ id: 'top-bar' },
                D.div({ className: 'title' },
                    D.a({ href: '/' },
                        D.img({src: '/img/mainLogo.png'})
                        // D.h1(null, this.props.isMobileOrSmall? 'M2C' : 'made2crash')
                    )
                ),
                userLogin,
                mobileBar,
                D.a(
                    { id: 'clickMe', className: 'icon' },
                    D.i(
                        { className: 'fa fa-bars' }
                    )
                )
                // D.div({ className: 'toggle-view noselect' + ((this.state.theme === 'white')? ' black' : ' white'), onClick: this._toggleTheme },
                //     D.a(null,
                //         (this.state.theme === 'white')? 'Go black' : 'Go back'
                //     )
                // ),
                // D.div({ className: 'full-screen noselect', onClick: this._toggleFullScreen },
                // 	 this.state.fullScreen? D.i({ className: 'fa fa-compress' }) : D.i({ className: 'fa fa-expand' })
            	// )
            )
        }
    });
});