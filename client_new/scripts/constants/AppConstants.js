define(['lib/key-mirror'], function(KeyMirror){

    return {

        ActionTypes: KeyMirror({

            //Game Actions
            PLACE_BET: null,
            PLACE_BET_SUCCESS: null,
            PLACE_BET_ERROR: null,
            CANCEL_BET: null,
            CASH_OUT: null,
            SAY_CHAT: null,

            //Strategy Actions
            RUN_STRATEGY: null,
            STOP_SCRIPT: null,
            UPDATE_SCRIPT: null,
            SELECT_STRATEGY: null,
            SET_WIDGET_STATE: null,

            //Tab Selector
            SELECT_TAB: null,

            //Controls Selector
            SELECT_CONTROL: null,
            TOGGLE_CONTROL: null,

            //Controls
            SET_BET_SIZE: null,
            SET_AUTO_CASH_OUT: null,

            //Chat
            SET_CHAT_INPUT_TEXT: null,
            IGNORE_USER: null,
            CLIENT_MESSAGE: null,
            APPROVE_USER: null,
            LIST_MUTED_USERS: null,
            SET_BOTS_DISPLAY_MODE: null,
            JOIN_CHANNEL: null,
            CLOSE_CURRENT_CHANNEL: null,

            //Game Settings
            // TOGGLE_THEME: null,
            SET_CONTROLS_SIZE: null,
            SET_GRAPH_MODE: null,
            SET_CONTROLS_POSITION: null,
            SET_PLAYER_LIST_SIZE: null,
            SET_LEFT_WIDGET: null,
            TOGGLE_HOYTKEYS_STATE: null,

            //Hotkeys
            DOUBLE_BET: null,
            HALF_BET: null,
            MIN_BET: null,
            MAX_BET: null,
            //PLACE_BET (GAME ACTIONS)

            //Chart
            SELECT_CHART: null
        }),

        PayloadSources: KeyMirror({
            VIEW_ACTION: null
        }),

        Engine: {
            STOP_PREDICTING_LAPSE: 300,
            HOST: (window.document.location.host === 'made2crash.com' || window.DEV_OTT) ? 'gs.made2crash.com:443' : window.document.location.host.replace(/:9094$/, ':9095'),
            CHAT_HOST: window.document.location.host,
            MAX_BET: 0.5e8 /** Max bet per game is 5,000 RVN, this will be calculated dynamically in the future, based on the invested amount in the casino **/
        },

        BetButton: {
            INITIAL_DISABLE_TIME: 500 //The time the button is disabled after cashing out and after the game crashes
        },

        Chat: {
            MAX_LENGTH: 500
        },

        PlayerList: {
            DEFAULT_PLAYER_LIST_SIZE: 50
        },

        Animations: {
            NYAN_CAT_TRIGGER_MS: 115129, //115129ms ~ 1000x // 11552ms ~ 2x
            MULTIPLIER_X2: 11552,
            //MULTIPLIER_X2: 1552,
            MULTIPLIER_X10: 38403,
            //MULTIPLIER_X10: 4403,
            MULTIPLIER_X100: 76753
            //MULTIPLIER_X100: 9753
        }

    }

});

