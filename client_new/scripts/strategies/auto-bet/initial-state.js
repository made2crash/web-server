define(function(){
    return {
        baseBet: 0.00001,
        autoCashAt: 2,
        onLossSelectedOpt: 'return_to_base', //Options: return_to_base, increase_bet_by
        onLossIncreaseQty: 2,
        onWinSelectedOpt: 'return_to_base', //Options: return_to_base, increase_bet_by
        onWinIncreaseQty: 2,
        invalidData: false,
        maxBetStop: 0.5 // Changed the max bet here to 0.5 RVN
    }
});