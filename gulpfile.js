const gulp = require('gulp');
const requirejs = require('requirejs');
const vinylPaths = require('vinyl-paths');
const del = require('del');
const minifyCss = require('gulp-clean-css');
const imageMin = require('gulp-imagemin');
const concat = require('gulp-concat');
const terser = require('gulp-terser');
const merge = require('merge-stream');
const es = require('event-stream');
const hash = require('gulp-hash');
const extend = require('gulp-extend');
const rename = require("gulp-rename");

const configJsonPath = './config/build-config.json'; //Here....

/** Delete old build folder and config file to be replaced by new build and config files **/
gulp.task('cleanBuild', () => {
    return merge(
        gulp.src("build", {allowEmpty:true})
            .pipe(vinylPaths(del)),
        gulp.src("config/build-config.json", {allowEmpty:true})
            .pipe(vinylPaths(del))
    );
});


/** ======================================== Old Client ======================================== **/
/** ============================================================================================ **/

/** RequireJS Optimizer options **/
const oldClientOpts = {
    baseUrl: './client_old/scripts',
    out: './build/old/scripts/main-old.js',
    name: 'lib/almond',
    mainConfigFile: './client_old/scripts/main.js',
    include: 'main',
    insertRequire: ['main'],
    removeCombined: false,
    optimize: "uglify2", //none
    generateSourceMaps: false, //TODO: true
    preserveLicenseComments: false
};

/** Minify the Javascript with requireJs optimizer **/
gulp.task('minifyJsOld', (callback) => {
    requirejs.optimize(oldClientOpts, (buildResponse) => {
        // console.log(`SUCCESSFULLY OPTIMIZED THE JS FILES BELOW:\n ${buildResponse}`);
        callback();
    }, (err) => {
        callback(err);
        console.error('[Error on require optimization for minifyJsOld]: ', err);
    });
});

// Old JS Minify Plugin
gulp.task('oldJsMin', (done) => {
    gulp.src('./build/old/scripts/main-old.js')
        .pipe(terser())
            .pipe(rename('main-old.js'))
                .pipe(gulp.dest('./build/old/scripts/'));
    done();
});

/** Minify game and landing css into build dir **/
gulp.task('minifyCssOld', () => {
    return merge(
        // Game CSS
        gulp.src('client_old/css/game.css')
            .pipe(minifyCss())
                .pipe(rename('css/game-old.css'))
                    .pipe(gulp.dest('build/old/')),
        // App CSS
        gulp.src('client_old/css/app.css')
            .pipe(minifyCss({ compatibility: 'ie8' }))
                .pipe(rename('css/app-old.css'))
                    .pipe(gulp.dest('build/old/'))
    );
});


/** Copy the necessary files to prod folder **/
gulp.task('copyAssetsOld', () => {
    return merge(
        gulp.src("client_old/img/**/*.*")
            .pipe(imageMin())
                .pipe(gulp.dest('build/old/img')),
        gulp.src('client_old/fonts/**/*.*')
            .pipe(gulp.dest('build/old/fonts')),
        gulp.src('client_old/css/fonts/*.*')
            .pipe(gulp.dest('build/old/css/fonts')),
        gulp.src('client_old/sounds/**/*.*')
            .pipe(gulp.dest('build/old/sounds')),
        gulp.src('client_old/lib/**/*.*')
            .pipe(gulp.dest('build/old/lib'))
    );
});


/** Hash the config.js and the app.css files  **/
const oldHashOptions = {
    template: '<%= name %>-<%= hash %><%= ext %>'
};

gulp.task('hash-css-game-old', () => {
    return addToManifest(
        gulp.src('./build/old/css/game-old.css')
            .pipe(hash(oldHashOptions))
                .pipe(gulp.dest('build/old/css'))
    );
});

gulp.task('hash-css-app-old', () => {
    return addToManifest(
        gulp.src('./build/old/css/app-old.css')
            .pipe(hash(oldHashOptions))
                .pipe(gulp.dest('build/old/css'))
    );
});

gulp.task('hash-js-old', () => {
    return addToManifest(
        gulp.src('./build/old/scripts/main-old.js')
            .pipe(hash(oldHashOptions))
                .pipe(gulp.dest('build/old/scripts'))
    );
});

gulp.task('hash-files-old', gulp.parallel('hash-css-game-old', 'hash-css-app-old', 'hash-js-old'), (done) => {
    done();
});


/** ======================================== New Client ======================================== **/
/** ============================================================================================ **/

/** RequireJS Optimizer options **/
const newClientOptions = {
    baseUrl: './client_new/scripts',
    out: './build/scripts/main-new.js',
    name: '../../node_modules/almond/almond',
    mainConfigFile: './client_new/scripts/main.js',
    include: 'main',
    insertRequire: ['main'],
    removeCombined: false,
    optimize: "uglify2", //none
    generateSourceMaps: false, //TODO: true
    preserveLicenseComments: false
};

/** Minify the Javascript with requireJs optizer **/
gulp.task('minifyJsNew', (callback) => {
    requirejs.optimize(newClientOptions, (buildResponse) => {
        // console.log(`SUCCESSFULLY OPTIMIZED THE JS FILES BELOW:\n ${buildResponse}`);
        callback();
    }, (err) => {
        callback(err);
        console.error('[Error on require optimization for minifyJsNew]: ', err);
    });
});

// New JS Minify Plugin
gulp.task('newJsMin', (done) => {
    gulp.src('./build/scripts/main-new.js')
        .pipe(terser())
            .pipe(rename('main-new.js'))
                .pipe(gulp.dest('./build/scripts/'));
    done();
});


/** Minify game and app css into build dir **/
gulp.task('minifyCssNew', function() {
    return merge(
        // GAME CSS...
        gulp.src('client_new/css/game.css')
            .pipe(minifyCss())
                .pipe(rename('css/game-new.css'))
                    .pipe(gulp.dest('build/')),
        //Game white theme css
        gulp.src('client_new/css/blackTheme.css')
            .pipe(minifyCss({ compatibility: 'ie8' }))
                .pipe(rename('css/game-theme-new.css'))
                    .pipe(gulp.dest('build/')),
        // APP CSS
        gulp.src('client_new/css/app.css')
            .pipe(minifyCss({ compatibility: 'ie8' }))
                .pipe(rename('css/app-new.css'))
                    .pipe(gulp.dest('build/'))
    )
});


/** Copy other files to build folder **/
gulp.task('copyAssetsNew', function() {
    return gulp.src('client_new/**/*.*')
        .pipe(gulp.dest('build/'));
});

/** Minify and Copy images to build folder **/
gulp.task('copyImgNew', (done) => {
    gulp.src("client_new/img/**/*.*")
        .pipe(imageMin())
            .pipe(gulp.dest('build/img'));
    done();
});


/** Hash the config.js and the app.css files  **/
const newHashOptions = {
    template: '<%= name %>-<%= hash %><%= ext %>'
};

gulp.task('hash-css-game-new', () => {
    return addToManifest(
        gulp.src('./build/css/game-new.css')
            .pipe(hash(newHashOptions))
                .pipe(gulp.dest('build/css'))
    );
});

gulp.task('hash-css-game-theme-new', () => {
    return addToManifest(
        gulp.src('./build/css/game-theme-new.css')
            .pipe(hash(newHashOptions))
                .pipe(gulp.dest('build/css'))
    );
});

gulp.task('hash-css-app-new', () => {
    return addToManifest(
        gulp.src('./build/css/app-new.css')
            .pipe(hash(newHashOptions))
                .pipe(gulp.dest('build/css'))
    );
});

gulp.task('hash-js-new', () => {
    return addToManifest(
        gulp.src('./build/scripts/main-new.js')
            .pipe(hash(newHashOptions))
                .pipe(gulp.dest('build/scripts'))
    );
});

gulp.task('hash-files-new', gulp.parallel('hash-css-game-new', 'hash-css-game-theme-new', 'hash-css-app-new', 'hash-js-new'), (done) => {
    done();
});



/** ======================================== Admin Dashboard Client ======================================== **/
/** ============================================================================================ **/

/** Minify the Javascript for Admin DashBoard **/
gulp.task('minifyJsAdmin', () => {
    return gulp.src('admin/js/*.js')
    .pipe(concat('main-admin.js'))
        .pipe(terser())
            .pipe(gulp.dest('build/admin/js'))
  });

/** Minify Admin css into build folder **/
gulp.task('minifyCssAdmin', () => {
    return merge(
        gulp.src('admin/css/app.css')
            .pipe(minifyCss({ compatibility: 'ie8' }))
                .pipe(rename('css/app-admin.css'))
                    .pipe(gulp.dest('build/admin/'))
    );
});

/** Copy the necessary files ADmin files to build folder **/
gulp.task('copyAssetsAdmin', () => {
    return merge(
       gulp.src('admin/images/*.*')
            .pipe(imageMin())
                .pipe(gulp.dest('build/admin/images')),
        gulp.src('admin/fonts/*.*')
            .pipe(gulp.dest('build/admin/fonts')),
        gulp.src('admin/js/*.*')
            .pipe(gulp.dest('build/admin/js')),
        gulp.src('admin/sounds/*.*')
            .pipe(gulp.dest('build/admin/sounds'))
    );
});

/** Hash the Admin files  **/
const adminHashOptions = {
    template: '<%= name %>-<%= hash %><%= ext %>'
};

gulp.task('hash-css-admin', () => {
    return addToManifest(
        gulp.src('./build/admin/css/app-admin.css')
            .pipe(hash(adminHashOptions))
                .pipe(gulp.dest('build/admin/css'))
    );
});

gulp.task('hash-js-admin', () => {
    return addToManifest(
        gulp.src('./build/admin/js/main-admin.js')
            .pipe(hash(adminHashOptions))
                .pipe(gulp.dest('build/admin/js'))
    );
});

gulp.task('hash-files-admin', gulp.parallel('hash-css-admin', 'hash-js-admin'), (done) => {
    done();
});


// Adds the files in `srcStream` to the manifest file, extending the manifest's current contents.
const addToManifest = (srcStream) => {
    return es.concat(
        gulp.src(configJsonPath, {allowEmpty:true}),
        srcStream
            .pipe(hash.manifest(configJsonPath))
    )
        .pipe(extend(configJsonPath, false, 4))
        .pipe(gulp.dest('.'));
}


// Run Build
gulp.task('Build', gulp.series(
    // First Task: Clean Build
    'cleanBuild',

    // Old FILES
    gulp.series('minifyJsOld'), gulp.series('oldJsMin'),
    gulp.parallel('minifyCssOld', 'copyAssetsOld'),
    'hash-files-old',

    // NEW FILES
    gulp.series('minifyJsNew'), gulp.series('newJsMin'),
    gulp.parallel('minifyCssNew', 'copyAssetsNew'), gulp.series('copyImgNew'),
    'hash-files-new',

    // ADMIN-DASHBOARD FILES
    gulp.series('minifyJsAdmin'),
    gulp.parallel('minifyCssAdmin', 'copyAssetsAdmin'),
    'hash-files-admin',

));