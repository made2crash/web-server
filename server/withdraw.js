let assert = require('assert');
let bc = require('./bitcoin_client');
let db = require('./database');
let request = require('request');
let config = require('../config/config');

module.exports = function(userId, userName, satoshis, withdrawalAddress, withdrawalId, callback) {
    let minWithdraw = config.MINING_FEE + 1e8;

    assert(typeof userId === 'number');
    assert(satoshis >= minWithdraw);
    assert(typeof withdrawalAddress === 'string');
    assert(typeof callback === 'function');

    db.makeWithdrawal(userId, userName, satoshis, withdrawalAddress, withdrawalId, function (err, fundingId) {
        if (err) {
            if (err.code === '23514')
                callback('NOT_ENOUGH_MONEY');
            else if(err.code === '23505')
                callback('SAME_WITHDRAWAL_ID');
            else
                callback(err);
            return;
        }

        assert(fundingId);

        let amountToSend = (satoshis - config.MINING_FEE) / 1e8;
        console.log(`User ==> ${userName} is getting ==> ${amountToSend}RVN, for placing a withdrawal of ==> ${satoshis/1e8}RVN`);

        return callback('FUNDING_QUEUED');
    });
};