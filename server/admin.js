let assert = require('assert');
let async = require('async');
let database = require('./database');
let config = require('../config/config');

/**
 * The req.user.admin is inserted in the user validation middleware
 */

exports.giveAway = function(req, res) {
    let user = req.user;
    assert(user.admin);
    res.render('giveaway', { user: user });
};

exports.giveAwayHandle = function(req, res, next) {
    let user = req.user;
    assert(user.admin);

    if (config.PRODUCTION) {
        let ref = req.get('Referer');
        if (!ref) return next(new Error('Possible xsfr')); //Interesting enough to log it as an error

        if (ref.lastIndexOf('https://www.made2crash.com/admin-giveaway', 0) !== 0)
            return next(new Error('Bad referrer got: ' + ref));
    }

    let giveAwayUsers = req.body.users.split(/\s+/);
    let bits = parseFloat(req.body.bits);

    if (!Number.isFinite(bits) || bits <= 0)
        return next('Problem with RVN...');

    let satoshis = Math.round(bits * 1e8);

    database.addRawGiveaway(giveAwayUsers, satoshis , function(err) {

        // Logging Each GiveAway sent to the user
        console.log(`${giveAwayUsers} was added to the giveaway and got ${satoshis/1e8}RVN`)

        if (err) return res.redirect('/admin-giveaway?err=' + err);

        res.redirect('/admin-giveaway?m=Done');
    });
};