let bitcoin = require('bitcoin');
let fs = require('fs');
let path = require('path');
let config = require('../config/config');

let client = new bitcoin.Client({
    host: config.BITCOIND_HOST,
    port: config.BITCOIND_PORT,
    user: config.BITCOIND_USER,
    pass: config.BITCOIND_PASS,
    timeout: 60000
});

module.exports = client;