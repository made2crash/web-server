let assert = require('assert');
let db = require('./database');
let request = require('request');

// Doesn't validate
module.exports = function(fundingId, transactionId) {

    assert(typeof fundingId === 'number');
    assert(typeof transactionId === 'string');   

    db.setFundingsWithdrawalTxid(fundingId, transactionId);

};