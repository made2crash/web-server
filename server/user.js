let assert = require('better-assert');
let async = require('async');
let bitcoinjs = require('bitcoinjs-lib');
let request = require('request');
let timeago = require('timeago');
let lib = require('./lib');
let database = require('./database');
let withdraw = require('./withdraw');
let explorer = require('./explorer')
let sendEmail = require('./sendEmail');
let speakeasy = require('speakeasy');
let qr = require('qr-image');
let uuid = require('uuid');
let _ = require('lodash');
let config = require('../config/config');

let sessionOptions = {
    httpOnly: true,
    secure : config.PRODUCTION
};

/**
 * POST
 * Public API
 * Register a user
 */
exports.register  = function(req, res, next) {
    let values = _.merge(req.body, { user: {} });
    let username = lib.removeNullsAndTrim(values.user.name);
    let password = lib.removeNullsAndTrim(values.user.password);
    let confirm = lib.removeNullsAndTrim(values.user.confirm);
    let email = lib.removeNullsAndTrim(values.user.email);
    let legalconsent = lib.removeNullsAndTrim(values.legalconsent);
    let ipAddress = req.ip;
    let userAgent = req.get('user-agent');

    let notValid = lib.isInvalidUsername(username);
    if (notValid) return res.render('register', { warning: 'username not valid because: ' + notValid, values: values.user });

    // stop new registrations of >16 char usernames
    if (username.length > 16)
        return res.render('register', { warning: 'Username is too long', values: values.user });

    notValid = lib.isInvalidPassword(password);
    if (notValid) {
        values.user.password = null;
        values.user.confirm = null;
        values.user.legalconsent = null;
        return res.render('register', { warning: 'password not valid because: ' + notValid, values: values.user });
    }

    notValid = lib.isInvalidEmail(email);
    if (notValid) {
        values.user.password = null;
        values.user.confirm = null;
        values.user.legalconsent = null;
        return res.render('register', { warning: 'email not valid because: ' + notValid, values: values.user });
    }

    // Ensure password and confirmation match
    if (password !== confirm) {
        values.user.password = null;
        values.user.confirm = null;
        values.user.legalconsent = null;
        return res.render('register', {
          warning: 'password and confirmation did not match',
          values: values.user
        });
    }

    // Ensure user has consented to the ToS
    if (!!!legalconsent) {
        values.user.password = null;
        values.user.confirm = null;
        values.user.legalconsent = null;
        return res.render('register', {
            warning: 'You must agree to the Terms of Service to continue.',
            values: values.user
        })
    }

    database.createUser(username, password, email, ipAddress, userAgent, function(err, sessionId) {
        if (err) {
            if (err === 'USERNAME_TAKEN') {
                values.user.name = null;
                values.user.password = null;
                values.user.confirm = null;
                values.user.legalconsent = null;
                return res.render('register', { warning: 'User name taken...', values: values.user});
            }
            return next(new Error('Unable to register user: \n' + err));
        }
        res.cookie('id', sessionId, sessionOptions);
        return res.redirect('/play?m=new');
    });
};

/**
 * POST
 * Public API
 * Login a user
 */
exports.login = function(req, res, next) {
    let username = lib.removeNullsAndTrim(req.body.username);
    let password = lib.removeNullsAndTrim(req.body.password);
    let otp = lib.removeNullsAndTrim(req.body.otp);
    let remember = !!req.body.remember;
    let ipAddress = req.ip;
    let userAgent = req.get('user-agent');

    if (!username || !password)
        return res.render('login', { warning: 'no username or password' });

    database.validateUser(username, password, otp, function(err, userId) {
        if (err) {
            console.log('[Login] Error for ', username, ' err: ', err);

            if (err === 'NO_USER')
                return res.render('login',{ warning: 'Username does not exist' });
            if (err === 'WRONG_PASSWORD')
                return res.render('login', { warning: 'Invalid password' });
            if (err === 'INVALID_OTP') {
                let warning = otp ? 'Invalid one-time password' : undefined;
                return res.render('login-mfa', { username: username, password: password, warning: warning });
            }
            return next(new Error('Unable to validate user ' + username + ': \n' + err));
        }
        assert(userId);

        database.createSession(userId, ipAddress, userAgent, remember, function(err, sessionId, expires) {
            if (err)
                return next(new Error('Unable to create session for userid ' + userId +  ':\n' + err));

            if(remember)
                sessionOptions.expires = expires;

            res.cookie('id', sessionId, sessionOptions);
            res.redirect('/');
        });
    });
};

/**
 * POST
 * Logged API
 * Logout the current user
 */
exports.logout = function(req, res, next) {
    let sessionId = req.cookies.id;
    let userId = req.user.id;

    assert(sessionId && userId);

    database.expireSessionsByUserId(userId, function(err) {
        if (err)
            return next(new Error('Unable to logout got error: \n' + err));
        res.redirect('/');
    });
};

/**
 * GET
 * Logged API
 * Shows the graph of the user profit and games
 */
exports.profile = function(req, res, next) {

    let user = req.user; //If logged here is the user info
    let username = lib.removeNullsAndTrim(req.params.name);

    let page = null;
    if (req.query.p) { //The page requested or last
        page = parseInt(req.query.p);
        if (!Number.isFinite(page) || page < 0)
            return next('Invalid page');
    }

    if (!username)
        return next('No username in profile');

    database.getPublicStats(username, function(err, stats) {
        if (err) {
            if (err === 'USER_DOES_NOT_EXIST')
               return next('User does not exist');
            else
                return next(new Error('Cant get public stats: \n' + err));
        }

        /**
         * Pagination
         * If the page number is undefined it shows the last page
         * If the page number is given it shows that page
         * It starts counting from zero
         */

        let resultsPerPage = 50;
        let pages = Math.floor(stats.games_played / resultsPerPage);

        if (page && page >= pages)
            return next('User does not have page ', page);

        // first page absorbs all overflow
        let firstPageResultCount = stats.games_played - ((pages-1) * resultsPerPage);

        let showing = page ? resultsPerPage : firstPageResultCount;
        let offset = page ? (firstPageResultCount + ((pages - page - 1) * resultsPerPage)) : 0 ;

        if (offset > 100000) {
          return next('Sorry we can\'t show games that far back :( ');
        }

        let tasks = [
            function(callback) {
                database.getUserNetProfitLast(stats.user_id, showing + offset, callback);
            },
            function(callback) {
                database.getUserPlays(stats.user_id, showing, offset, callback);
            }
        ];


        async.parallel(tasks, function(err, results) {
            if (err) return next(new Error('Error getting user profit: \n' + err));

            let lastProfit = results[0];

            let netProfitOffset = stats.net_profit - lastProfit;
            let plays = results[1];


            if (!lib.isInt(netProfitOffset))
                return next(new Error('Internal profit calc error: ' + username + ' does not have an integer net profit offset'));

            assert(plays);

            plays.forEach(function(play) {
                play.timeago = timeago(play.created);
            });

            let previousPage;
            if (pages > 1) {
                if (page && page >= 2)
                    previousPage = '?p=' + (page - 1);
                else if (!page)
                    previousPage = '?p=' + (pages - 1);
            }

            let nextPage;
            if (pages > 1) {
                if (page && page < (pages-1))
                    nextPage ='?p=' + (page + 1);
                else if (page && page == pages-1)
                    nextPage = stats.username;
            }

            res.render('user', {
                user: user,
                stats: stats,
                plays: plays,
                net_profit_offset: netProfitOffset,
                showing_last: !!page,
                previous_page: previousPage,
                next_page: nextPage,
                games_from: stats.games_played-(offset + showing - 1),
                games_to: stats.games_played-offset,
                pages: {
                    current: page == 0 ? 1 : page + 1 ,
                    total: Math.ceil(stats.games_played / 100)
                }
            });
        });

    });
};

/**
 * GET
 * Shows the request bits page
 * Restricted API to logged users
 **/
// exports.request = function(req, res) {
//     let user = req.user; //Login var
//     assert(user);
//
//     res.render('request', { user: user });
// };

/**
 * POST
 * Process the give away requests
 * Restricted API to logged users
 **/
exports.giveawayRequest = function(req, res, next) {
    let user = req.user;
    assert(user);

    database.addGiveaway(user.id, function(err) {
        if (err) {
            if (err.message === 'NOT_ELIGIBLE') {
                return res.render('request', { user: user, warning: 'You have to wait ' + err.time + ' minutes for your next give away.' });
            } else if(err === 'USER_DOES_NOT_EXIST') {
                return res.render('error', { error: 'User does not exist.' });
            }

            return next(new Error('Unable to add giveaway: \n' + err));
        }
        user.eligible = 240;
        user.balance_satoshis += 200;
        return res.redirect('/play?m=received');
    });

};

/**
 * GET
 * Restricted API
 * Shows the account page, the default account page.
 **/
exports.account = function(req, res, next) {
    let user = req.user;
    assert(user);

    let tasks = [
        function(callback) {
            database.getDepositsAmount(user.id, callback);
        },
        function(callback) {
            database.getWithdrawalsAmount(user.id, callback);
        },
        function(callback) {
            database.getGiveAwaysAmount(user.id, callback);
        },
        function(callback) {
            database.getUserNetProfit(user.id, callback)
        }
    ];

    async.parallel(tasks, function(err, ret) {
        if (err)
            return next(new Error('Unable to get account info: \n' + err));

        let deposits = ret[0];
        let withdrawals = ret[1];
        let giveaways = ret[2];
        let net = ret[3];
        user.deposits = !deposits.sum ? 0 : deposits.sum;
        user.withdrawals = !withdrawals.sum ? 0 : withdrawals.sum;
        user.giveaways = !giveaways.sum ? 0 : giveaways.sum;
        user.net_profit = net.profit;
        user.deposit_address = lib.deriveAddress(user.id);

        res.render('account', { user: user });
    });
};

/**
 * POST
 * Restricted API
 * Change the user's password
 **/
exports.resetPassword = function(req, res, next) {
    let user = req.user;
    assert(user);
    let password = lib.removeNullsAndTrim(req.body.old_password);
    let newPassword = lib.removeNullsAndTrim(req.body.password);
    let otp = lib.removeNullsAndTrim(req.body.otp);
    let confirm = lib.removeNullsAndTrim(req.body.confirmation);
    let ipAddress = req.ip;
    let userAgent = req.get('user-agent');

    if (!password) return  res.redirect('/security?err=Enter%20your%20old%20password');

    let notValid = lib.isInvalidPassword(newPassword);
    if (notValid) return res.redirect('/security?err=new%20password%20not%20valid:' + notValid);

    if (newPassword !== confirm) return  res.redirect('/security?err=new%20password%20and%20confirmation%20should%20be%20the%20same.');

    database.validateUser(user.username, password, otp, function(err, userId) {
        if (err) {
            if (err  === 'WRONG_PASSWORD') return  res.redirect('/security?err=wrong password.');
            if (err === 'INVALID_OTP') return res.redirect('/security?err=invalid one-time password.');
            //Should be an user here
            return next(new Error('Unable to reset password: \n' + err));
        }
        assert(userId === user.id);
        database.changeUserPassword(user.id, newPassword, function(err) {
            if (err)
                return next(new Error('Unable to change user password: \n' +  err));

            database.expireSessionsByUserId(user.id, function(err) {
                if (err)
                    return next(new Error('Unable to delete user sessions for userId: ' + user.id + ': \n' + err));

                database.createSession(user.id, ipAddress, userAgent, false, function(err, sessionId) {
                    if (err)
                        return next(new Error('Unable to create session for userid ' + userId +  ':\n' + err));

                    res.cookie('id', sessionId, sessionOptions);
                    res.redirect('/security?m=Password changed');
                });
            });
        });
    });
};

/**
 * POST
 * Restricted API
 * Adds an email to the account
 **/
exports.editEmail = function(req, res, next) {
    let user  = req.user;
    assert(user);

    let email = lib.removeNullsAndTrim(req.body.email);
    let password = lib.removeNullsAndTrim(req.body.password);
    let otp = lib.removeNullsAndTrim(req.body.otp);

    //If no email set to null
    if(email.length === 0) {
        email = null;
    } else {
        let notValid = lib.isInvalidEmail(email);
        if (notValid) return res.redirect('/security?err=email invalid because: ' + notValid);
    }

    notValid = lib.isInvalidPassword(password);
    if (notValid) return res.render('/security?err=password not valid because: ' + notValid);

    database.validateUser(user.username, password, otp, function(err, userId) {
        if (err) {
            if (err === 'WRONG_PASSWORD') return res.redirect('/security?err=wrong%20password');
            if (err === 'INVALID_OTP') return res.redirect('/security?err=invalid%20one-time%20password');
            //Should be an user here
            return next(new Error('Unable to validate user adding email: \n' + err));
        }

        database.updateEmail(userId, email, function(err) {
            if (err)
                return next(new Error('Unable to update email: \n' + err));

            res.redirect('security?m=Email added');
        });
    });
};

/**
 * GET
 * Restricted API
 * Shows the security page of the users account
 **/
exports.security = function(req, res) {
    let user = req.user;
    assert(user);

    if (!user.mfa_secret) {
        user.mfa_potential_secret = speakeasy.generate_key({ length: 32 }).base32;
        let qrUri = 'otpauth://totp/made2crash:' + user.username + '?secret=' + user.mfa_potential_secret + '&issuer=made2crash';
        user.qr_svg = qr.imageSync(qrUri, { type: 'svg' });
        user.sig = lib.sign(user.username + '|' + user.mfa_potential_secret);
    }

    res.render('security', { user: user });
};

/**
 * POST
 * Restricted API
 * Enables the two factor authentication
 **/
exports.enableMfa = function(req, res, next) {
    let user = req.user;
    assert(user);

    let otp = lib.removeNullsAndTrim(req.body.otp);
    let sig = lib.removeNullsAndTrim(req.body.sig);
    let secret = lib.removeNullsAndTrim(req.body.mfa_potential_secret);

    if (user.mfa_secret) return res.redirect('/security?err=2FA%20is%20already%20enabled');
    if (!otp) return next('Missing otp in enabling mfa');
    if (!sig) return next('Missing sig in enabling mfa');
    if (!secret) return next('Missing secret in enabling mfa');

    if (!lib.validateSignature(user.username + '|' + secret, sig))
        return next('Could not validate sig');

    let expected = speakeasy.totp({ key: secret, encoding: 'base32' });

    if (otp !== expected) {
        user.mfa_potential_secret = secret;
        let qrUri = 'otpauth://totp/made2crash:' + user.username + '?secret=' + secret + '&issuer=made2crash';
        user.qr_svg = qr.imageSync(qrUri, {type: 'svg'});
        user.sig = sig;

        return res.render('security', { user: user, warning: 'Invalid 2FA token' });
    }

    database.updateMfa(user.id, secret, function(err) {
        if (err) return next(new Error('Unable to update 2FA status: \n' + err));
        res.redirect('/security?=m=Two-Factor%20Authentication%20Enabled');
    });
};

/**
 * POST
 * Restricted API
 * Disables the two factor authentication
 **/
exports.disableMfa = function(req, res, next) {
    let user = req.user;
    assert(user);

    let secret = lib.removeNullsAndTrim(user.mfa_secret);
    let otp = lib.removeNullsAndTrim(req.body.otp);

    if (!secret) return res.redirect('/security?err=Did%20not%20sent%20mfa%20secret');
    if (!user.mfa_secret) return res.redirect('/security?err=2FA%20is%20not%20enabled');
    if (!otp) return res.redirect('/security?err=No%20OTP');

    let expected = speakeasy.totp({ key: secret, encoding: 'base32' });

    if (otp !== expected)
        return res.redirect('/security?err=invalid%20one-time%20password');

    database.updateMfa(user.id, null, function(err) {
        if (err) return next(new Error('Error updating Mfa: \n' + err));

        res.redirect('/security?m=Two-Factor%20Authentication%20Disabled');
    });
};

/**
 * POST
 * Restricted API
 * Modifies the Discord ID of a user
 */
exports.changeDiscordID = function(req, res, next) {
    let user = req.user;
    assert(user);

    let discordId = req.body.discordId !== null && req.body.discordId !== undefined ? req.body.discordId : ''

    database.updateDiscordID(user.id, discordId, function (err) {
        if (err) return next(new Error('Error updating discord id: \n' + err));
        let message = encodeURIComponent('Discord ID Updated Successfully')
        res.redirect('/security?m=' + message);
    })
}
/**
 * POST
 * Public API
 * Send password recovery to an user if possible
 **/
exports.sendPasswordRecover = function(req, res, next) {
    let email = lib.removeNullsAndTrim(req.body.email);
    if (!email) return res.redirect('forgot-password');
    let remoteIpAddress = req.ip;

    //We don't want to leak if the email has users, so we send this message even if there are no users from that email
    let messageSent = { success: `We\'ve sent an email to your email: ${email}, if there is a recovery email.` };

    database.getUsersFromEmail(email, function(err, users) {
        if(err) {
            if(err === 'NO_USERS')
                return res.render('forgot-password', messageSent);
            else
                return next(new Error('Unable to get users by email ' + email +  ': \n' + err));
        }

        let recoveryList = []; //An array of pairs [username, recoveryId]
        async.each(users, function(user, callback) {

            database.addRecoverId(user.id, remoteIpAddress, function(err, recoveryId) {
                if(err)
                    return callback(err);

                recoveryList.push([user.username, recoveryId]);
                callback(); //async success
            })

        }, function(err) {
            if(err)
                return next(new Error('Unable to add recovery id :\n' + err));

            sendEmail.passwordReset(email, recoveryList, function(err) {
                if(err)
                    return next(new Error('Unable to send password email: \n' + err));

                return res.render('forgot-password',  messageSent);
            });
        });

    });
};

/**
 * GET
 * Public API
 * Validate if the reset id is valid or is has not being uses, does not alters the recovery state
 * Renders the change password
 **/
exports.validateResetPassword = function(req, res, next) {
    let recoverId = req.params.recoverId;
    if (!recoverId || !lib.isUUIDv4(recoverId))
        return next('Invalid recovery id');

    database.getUserByValidRecoverId(recoverId, function(err, user) {
        if (err) {
            if (err === 'NOT_VALID_RECOVER_ID')
                return next('Invalid recovery id');
            return next(new Error('Unable to get user by recover id ' + recoverId + '\n' + err));
        }
        res.render('reset-password', { user: user, recoverId: recoverId });
    });
};

/**
 * POST
 * Public API
 * Receives the new password for the recovery and change it
 **/
exports.resetPasswordRecovery = function(req, res, next) {
    let recoverId = req.body.recover_id;
    let password = lib.removeNullsAndTrim(req.body.password);
    let ipAddress = req.ip;
    let userAgent = req.get('user-agent');

    if (!recoverId || !lib.isUUIDv4(recoverId)) return next('Invalid recovery id');

    let notValid = lib.isInvalidPassword(password);
    if (notValid) return res.render('reset-password', { recoverId: recoverId, warning: 'password not valid because: ' + notValid });

    database.changePasswordFromRecoverId(recoverId, password, function(err, user) {
        if (err) {
            if (err === 'NOT_VALID_RECOVER_ID')
                return next('Invalid recovery id');
            return next(new Error('Unable to change password for recoverId ' + recoverId + ', password: ' + password + '\n' + err));
        }
        database.createSession(user.id, ipAddress, userAgent, false, function(err, sessionId) {
            if (err)
                return next(new Error('Unable to create session for password from recover id: \n' + err));

            res.cookie('id', sessionId, sessionOptions);
            res.redirect('/');
        });
    });
};

/**
 * GET
 * Restricted API
 * Shows the deposit history
 **/
exports.deposit = function(req, res, next) {
    let user = req.user;
    assert(user);

    database.getDeposits(user.id, function(err, deposits) {
        if (err) {
            return next(new Error('Unable to get deposits: \n' + err));
        }
        user.deposits = deposits;
        user.deposit_address = lib.deriveAddress(user.id);
        res.render('deposit', { user:  user });
    });
};

/**
 * GET
 * Restricted API
 * Shows the withdrawal history
 **/
exports.withdraw = function(req, res, next) {
    let user = req.user;
    assert(user);

    database.getWithdrawals(user.id, function(err, withdrawals) {
        if (err)
            return next(new Error('Unable to get withdrawals: \n' + err));

        withdrawals.forEach(function(withdrawal) {
            withdrawal.shortDestination = withdrawal.destination.substring(0,8);
        });
        user.withdrawals = withdrawals;

        res.render('withdraw', { user: user });
    });
};

/**
 * POST
 * Restricted API
 * Process a withdrawal
 **/
exports.handleWithdrawRequest = function(req, res, next) {
    let user = req.user;
    assert(user);

    let userName = req.body.username;
    let amount = req.body.amount;
    let destination = req.body.destination;
    let withdrawalId = req.body.withdrawal_id;
    let password = lib.removeNullsAndTrim(req.body.password);
    let otp = lib.removeNullsAndTrim(req.body.otp);

    let r =  /^[1-9]\d*(\.\d{0,8})?$/;
    if (!r.test(amount))
        return res.render('withdraw-request', { user: user, id: uuid.v4(),  warning: 'Not a valid amount' });

    console.log("Withdrawal Amount before Conversion in RVN is: " + amount + "RVN");
    
    amount = Math.round(parseFloat(amount) * 1e8);

    console.log("Withdrawal Amount after COnversion to Satoshi is " + amount);
    assert(Number.isFinite(amount));

    let minWithdraw = config.MINING_FEE + 1e8; // Added 1 RVN to the minimum withdraw

    if (amount < minWithdraw)
        return res.render('withdraw-request', { user: user,  id: uuid.v4(), warning: 'You must withdraw ' + minWithdraw + ' or more'  });

    if (typeof destination !== 'string')
        return res.render('withdraw-request', { user: user,  id: uuid.v4(), warning: 'Destination address not provided' });

    try {
        let version = bitcoinjs.Address.fromBase58Check(destination).version;
        if (version !== bitcoinjs.networks.ravencoin.pubKeyHash && version !== bitcoinjs.networks.ravencoin.scriptHash)
            return res.render('withdraw-request', { user: user,  id: uuid.v4(), warning: 'Destination address is not a ravencoin one' });
    } catch(ex) {
        return res.render('withdraw-request', { user: user,  id: uuid.v4(), warning: 'Not a valid destination address' });
    }

    if (!password)
        return res.render('withdraw-request', { user: user,  id: uuid.v4(), warning: 'Must enter a password' });

    if(!lib.isUUIDv4(withdrawalId))
      return res.render('withdraw-request', { user: user,  id: uuid.v4(), warning: 'Could not find a one-time token' });

    database.validateUser(user.username, password, otp, function(err) {

        if (err) {
            if (err === 'WRONG_PASSWORD')
                return res.render('withdraw-request', { user: user, id: uuid.v4(), warning: 'wrong password, try it again...' });
            if (err === 'INVALID_OTP')
                return res.render('withdraw-request', { user: user, id: uuid.v4(), warning: 'invalid one-time token' });
            //Should be a user
            return next(new Error('Unable to validate user handling withdrawal: \n' + err));
        }

        withdraw(req.user.id, userName, amount, destination, withdrawalId, function(err) {
            if (err) {
                if (err === 'NOT_ENOUGH_MONEY')
                    return res.render('withdraw-request', { user: user, id: uuid.v4(), warning: 'Not enough money to process withdraw.' });
                else if (err === 'PENDING')
                    return res.render('withdraw-request', { user: user,  id: uuid.v4(), success: 'Withdrawal successful, however hot wallet was empty. Withdrawal will be reviewed and sent ASAP' });
                else if(err === 'SAME_WITHDRAWAL_ID')
                    return res.render('withdraw-request', { user: user,  id: uuid.v4(), warning: 'Please reload your page, it looks like you tried to make the same transaction twice.' });
                else if(err === 'FUNDING_QUEUED')
                    return res.render('withdraw-request', { user: user,  id: uuid.v4(), success: 'Your transaction is being processed come back later to see the status.' });
                else
                    return next(new Error('Unable to withdraw: ' + err));
            }
            return res.render('withdraw-request', { user: user, id: uuid.v4(), success: 'OK' });
        });
    });
};

/**
 * GET
 * Restricted API
 * Shows the withdrawal request page
 **/
exports.withdrawRequest = function(req, res) {
    assert(req.user);
    res.render('withdraw-request', { user: req.user, id: uuid.v4() });
};


/**
 * GET
 * Restricted API
 * Shows the all transaction withdrawal history on the transaction explorer page
 **/
exports.explorer = function(req, res, next) {
    let user = req.user;
    assert(user);

    database.getTransactionExplorer(function(err, transactions) {
        if (err)
            return next(new Error('Unable to get Transactions Explorer: \n' + err));

            transactions.forEach(transaction => {
            transaction.shortDestination = transaction.destination.substring(0,8);

        });
        user.transactions = transactions;

        res.render('explorer', { user: user });
    });
};

/**
 * POST
 * Restricted API
 * Process a transaction as Approved or Decline
 **/
exports.handleTransaction = function(req, res, next) {
    let user = req.user;
    assert(user);

    let fundingId = req.body.fundingId;
    fundingId = parseInt(fundingId);
    let transactionId = req.body.transactionId;

        explorer(fundingId, transactionId);

        return res.redirect('/portal/explorer?m=Transaction Approved Successfully !!!');
};

/**
 * GET
 * Restricted API
 * Shows the all transaction withdrawal history on the transaction explorer page
 **/
exports.editUnterminatedGames = function(req, res, next) {
    let user = req.user;
    assert(user);

    database.getUnterminatedGames((err, unterminatedGames) => {
        if (err)
            return next(new Error('Unable to get Un-Terminated Games: \n' + err));

        user.unterminatedGames = unterminatedGames.filter((d, index) => {
            return index !== unterminatedGames.length-1;
        });

        res.render('edit-unterminated-games', { user: user });
    });
};


/**
 * POST
 * Restricted API
 * Process a transaction as Approved or 
 **/
exports.handleEditUnterminatedGames = function(req, res, next) {
    let user = req.user;
    assert(user);

    let gameId = req.body.gameId;
    gameId = parseInt(gameId);
    let ended = req.body.ended;

    editUnterminatedGames(gameId, ended);

        return res.redirect('/portal/edit-unterminated-games?m=Game Edited Successfully !!!');
};

/**
 * GET
 * Restricted API
 * Shows the support page
 **/
exports.portal = function(req, res) {
    assert(req.user);
    res.render('portal', { user: req.user })
};


/**
 * GET
 * Restricted API
 * Shows the support page
 **/
exports.contact = function(req, res) {
    assert(req.user);
    res.render('support', { user: req.user })
};

/**
 * GET
 * Public API
 * Returns an array of usernames or null
 **/
exports.getUsernamesByPrefix = function(req, res, next) {
    let prefix = req.params.prefix;

    //Validate prefix
    if(lib.isInvalidUsername(prefix))
        return res.status(400).send('INVALID_PREFIX');

    database.getUsernamesByPrefix(prefix, function(err, usernames) {
        if(err) {
            console.error('[INTERNAL_ERROR] unable to request usernames by prefix: ', usernames);
            return res.status(500).send('INTERNAL_ERROR');
        }

        res.send(JSON.stringify(usernames));
    })
};