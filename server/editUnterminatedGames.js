const assert = require('assert');
const db = require('./database');
const request = require('request');


module.exports = function(gameId, ended) {

    assert(typeof gameId === 'number');
    assert(typeof ended === 'string');   

    db.setUnterminatedGames(gameId, ended);

};