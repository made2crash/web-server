let fs = require('fs');
let production = process.env.NODE_ENV === 'production';

let prodConfig;
if(production) {
  prodConfig = JSON.parse(fs.readFileSync(__dirname + '/build-config.json'));
  console.log('Build config loaded: ', prodConfig);
}

module.exports = {
  "PRODUCTION": production,
  "DATABASE_URL": process.env.DATABASE_URL || "postgres://localhost:5432/bustabitdb",
  "BIP32_DERIVED": process.env.BIP32_DERIVED_KEY,
  "AWS_SES_KEY": process.env.AWS_SES_KEY,
  "AWS_SES_SECRET": process.env.AWS_SES_SECRET,
  "CONTACT_EMAIL": process.env.CONTACT_EMAIL || "made2crash@gmail.com",
  "SITE_URL": process.env.SITE_URL || '',
  "ENC_KEY": process.env.ENC_KEY || "devkey",
  "SIGNING_SECRET": process.env.WS_SIGNING_SECRET || "secret",
  "BANKROLL_OFFSET": process.env.BANKROLL_OFFSET || 0,
  "RECAPTCHA_PRIV_KEY": process.env.RECAPTCHA_PRIV_KEY || '',
  "RECAPTCHA_SITE_KEY": process.env.RECAPTCHA_SITE_KEY || '',
  "BITCOIND_HOST": process.env.BITCOIND_HOST,
  "BITCOIND_PORT": process.env.BITCOIND_PORT || 0,
  "BITCOIND_USER": process.env.BITCOIND_USER,
  "BITCOIND_PASS": process.env.BITCOIND_PASS,
  "PORT":  process.env.WS_PORT || 0,
  "MINING_FEE": parseFloat(process.env.MINING_FEE) || 2e8, // The Mining Fee is 2 RVN...
  "BUILD": prodConfig
};
